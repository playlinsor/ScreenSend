﻿using System;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace ScreenSend
{
    class HookKey
    {
        //Тут объявим WinApi
        [DllImport("user32.dll")]
        private static extern bool RegisterHotKey(IntPtr hWnd, int id, uint fsModifiers, uint vk);
        [DllImport("user32.dll")]
        private static extern bool UnregisterHotKey(IntPtr hWnd, int id);

        public delegate void EventHookKey();
        public event EventHookKey OnHookKey = null;

        [Flags]
        public enum ModiferKeys : uint
        {
            Alt = 1,Control = 2,Shift = 4,Win = 8
        }

        IntPtr CurrentWinHandle;

        public void SetHooK(uint HookControl, uint HookKey)
        {
            UnregisterHotKey(CurrentWinHandle, 0);
            RegisterHotKey(CurrentWinHandle, 0, HookControl, HookKey);
        }

        public HookKey(IntPtr WinHandle,uint HookControl,uint HookKey)
        {
            //(int)ModiferKeys.Control
            RegisterHotKey(WinHandle, 0, HookControl, HookKey);
            CurrentWinHandle = WinHandle;
        }

        /* Not Work (WndProc on main)
        protected void WndProc(ref Message m)
        {
            if (m.Msg == 0x0312)
            {
                if (m.WParam.ToInt32() == 0)
                {
                    OnHookKey();
                }
            }
        }
        */
        ~HookKey()
        {
            UnregisterHotKey(CurrentWinHandle, 0);
        }
       
    }
}
